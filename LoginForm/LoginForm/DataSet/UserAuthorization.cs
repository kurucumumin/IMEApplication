//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LoginForm.DataSet
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserAuthorization
    {
        public int WorkerID { get; set; }
        public int AuthorizationID { get; set; }
        public int AuthRoleID { get; set; }
    
        public virtual AuthorizationValue AuthorizationValue { get; set; }
        public virtual AuthRole AuthRole { get; set; }
        public virtual Worker Worker { get; set; }
    }
}
