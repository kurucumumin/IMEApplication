﻿namespace LoginForm
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.tableLeftPanel = new System.Windows.Forms.TableLayoutPanel();
            this.gbMainMenu = new System.Windows.Forms.GroupBox();
            this.btnDevelopment = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnLoader = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableRightPanel = new System.Windows.Forms.TableLayoutPanel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.btnTSEList = new System.Windows.Forms.Button();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.btnExtendedRangePrice = new System.Windows.Forms.Button();
            this.panel16 = new System.Windows.Forms.Panel();
            this.btnHazardousFile = new System.Windows.Forms.Button();
            this.btnItemCard = new System.Windows.Forms.Button();
            this.panel15 = new System.Windows.Forms.Panel();
            this.btnDualUsedArticles = new System.Windows.Forms.Button();
            this.btnWorker = new System.Windows.Forms.Button();
            this.panel14 = new System.Windows.Forms.Panel();
            this.btnSuperDiskwithP = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnDiscontinuedList = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnSlidingPriceList = new System.Windows.Forms.Button();
            this.btnSupplier = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnSuperDisk = new System.Windows.Forms.Button();
            this.btnQuotation = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnOnSale = new System.Windows.Forms.Button();
            this.btnCustomer = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dock1 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.btnRSProList = new System.Windows.Forms.Button();
            this.tableLeftPanel.SuspendLayout();
            this.gbMainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tableMain.SuspendLayout();
            this.tableRightPanel.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel21.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLeftPanel
            // 
            this.tableLeftPanel.ColumnCount = 1;
            this.tableLeftPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLeftPanel.Controls.Add(this.gbMainMenu, 0, 1);
            this.tableLeftPanel.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLeftPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLeftPanel.Location = new System.Drawing.Point(2, 2);
            this.tableLeftPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tableLeftPanel.Name = "tableLeftPanel";
            this.tableLeftPanel.RowCount = 2;
            this.tableLeftPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 122F));
            this.tableLeftPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLeftPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLeftPanel.Size = new System.Drawing.Size(146, 354);
            this.tableLeftPanel.TabIndex = 0;
            // 
            // gbMainMenu
            // 
            this.gbMainMenu.Controls.Add(this.btnDevelopment);
            this.gbMainMenu.Controls.Add(this.button5);
            this.gbMainMenu.Controls.Add(this.button4);
            this.gbMainMenu.Controls.Add(this.button3);
            this.gbMainMenu.Controls.Add(this.button2);
            this.gbMainMenu.Controls.Add(this.btnLoader);
            this.gbMainMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbMainMenu.Location = new System.Drawing.Point(2, 124);
            this.gbMainMenu.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbMainMenu.Name = "gbMainMenu";
            this.gbMainMenu.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.gbMainMenu.Size = new System.Drawing.Size(141, 210);
            this.gbMainMenu.TabIndex = 30;
            this.gbMainMenu.TabStop = false;
            this.gbMainMenu.Text = "Menu";
            // 
            // btnDevelopment
            // 
            this.btnDevelopment.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDevelopment.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDevelopment.Location = new System.Drawing.Point(2, 178);
            this.btnDevelopment.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDevelopment.Name = "btnDevelopment";
            this.btnDevelopment.Size = new System.Drawing.Size(137, 32);
            this.btnDevelopment.TabIndex = 34;
            this.btnDevelopment.Text = "Developments";
            this.btnDevelopment.UseVisualStyleBackColor = true;
            this.btnDevelopment.Click += new System.EventHandler(this.btnDevelopment_Click);
            // 
            // button5
            // 
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(2, 146);
            this.button5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(137, 32);
            this.button5.TabIndex = 33;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(2, 114);
            this.button4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(137, 32);
            this.button4.TabIndex = 32;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(2, 82);
            this.button3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(137, 32);
            this.button3.TabIndex = 31;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(2, 50);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(137, 32);
            this.button2.TabIndex = 30;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btnLoader
            // 
            this.btnLoader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLoader.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLoader.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoader.Location = new System.Drawing.Point(2, 18);
            this.btnLoader.Margin = new System.Windows.Forms.Padding(2, 2, 2, 13);
            this.btnLoader.Name = "btnLoader";
            this.btnLoader.Size = new System.Drawing.Size(137, 32);
            this.btnLoader.TabIndex = 29;
            this.btnLoader.Text = "Loader";
            this.btnLoader.UseVisualStyleBackColor = true;
            this.btnLoader.Click += new System.EventHandler(this.btnLoader_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::LoginForm.Properties.Resources.IME;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(2, 2);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(142, 118);
            this.pictureBox1.TabIndex = 31;
            this.pictureBox1.TabStop = false;
            // 
            // tableMain
            // 
            this.tableMain.ColumnCount = 2;
            this.tableMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableMain.Controls.Add(this.tableLeftPanel, 0, 0);
            this.tableMain.Controls.Add(this.tableRightPanel, 1, 0);
            this.tableMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableMain.Location = new System.Drawing.Point(0, 0);
            this.tableMain.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tableMain.Name = "tableMain";
            this.tableMain.RowCount = 1;
            this.tableMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableMain.Size = new System.Drawing.Size(850, 358);
            this.tableMain.TabIndex = 0;
            // 
            // tableRightPanel
            // 
            this.tableRightPanel.ColumnCount = 6;
            this.tableRightPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableRightPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableRightPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableRightPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableRightPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableRightPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableRightPanel.Controls.Add(this.panel30, 0, 4);
            this.tableRightPanel.Controls.Add(this.panel29, 0, 4);
            this.tableRightPanel.Controls.Add(this.panel28, 0, 4);
            this.tableRightPanel.Controls.Add(this.panel27, 0, 4);
            this.tableRightPanel.Controls.Add(this.panel26, 0, 4);
            this.tableRightPanel.Controls.Add(this.panel25, 0, 4);
            this.tableRightPanel.Controls.Add(this.panel20, 1, 3);
            this.tableRightPanel.Controls.Add(this.panel18, 5, 2);
            this.tableRightPanel.Controls.Add(this.panel17, 4, 2);
            this.tableRightPanel.Controls.Add(this.panel16, 3, 2);
            this.tableRightPanel.Controls.Add(this.panel15, 2, 2);
            this.tableRightPanel.Controls.Add(this.panel14, 1, 2);
            this.tableRightPanel.Controls.Add(this.panel13, 0, 2);
            this.tableRightPanel.Controls.Add(this.panel12, 5, 1);
            this.tableRightPanel.Controls.Add(this.panel11, 4, 1);
            this.tableRightPanel.Controls.Add(this.panel9, 3, 1);
            this.tableRightPanel.Controls.Add(this.panel8, 2, 1);
            this.tableRightPanel.Controls.Add(this.panel7, 1, 1);
            this.tableRightPanel.Controls.Add(this.panel6, 0, 1);
            this.tableRightPanel.Controls.Add(this.panel5, 5, 0);
            this.tableRightPanel.Controls.Add(this.panel4, 4, 0);
            this.tableRightPanel.Controls.Add(this.panel3, 3, 0);
            this.tableRightPanel.Controls.Add(this.panel2, 2, 0);
            this.tableRightPanel.Controls.Add(this.panel1, 1, 0);
            this.tableRightPanel.Controls.Add(this.dock1, 0, 0);
            this.tableRightPanel.Controls.Add(this.panel19, 0, 3);
            this.tableRightPanel.Controls.Add(this.panel24, 5, 3);
            this.tableRightPanel.Controls.Add(this.panel23, 4, 3);
            this.tableRightPanel.Controls.Add(this.panel22, 3, 3);
            this.tableRightPanel.Controls.Add(this.panel21, 2, 3);
            this.tableRightPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableRightPanel.Location = new System.Drawing.Point(152, 2);
            this.tableRightPanel.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tableRightPanel.Name = "tableRightPanel";
            this.tableRightPanel.RowCount = 5;
            this.tableRightPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableRightPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableRightPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableRightPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableRightPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableRightPanel.Size = new System.Drawing.Size(696, 354);
            this.tableRightPanel.TabIndex = 1;
            // 
            // panel30
            // 
            this.panel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel30.Location = new System.Drawing.Point(2, 282);
            this.panel30.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(111, 70);
            this.panel30.TabIndex = 61;
            // 
            // panel29
            // 
            this.panel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel29.Location = new System.Drawing.Point(117, 282);
            this.panel29.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(111, 70);
            this.panel29.TabIndex = 60;
            // 
            // panel28
            // 
            this.panel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel28.Location = new System.Drawing.Point(462, 282);
            this.panel28.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(111, 70);
            this.panel28.TabIndex = 59;
            // 
            // panel27
            // 
            this.panel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel27.Location = new System.Drawing.Point(577, 282);
            this.panel27.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(117, 70);
            this.panel27.TabIndex = 58;
            // 
            // panel26
            // 
            this.panel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel26.Location = new System.Drawing.Point(232, 282);
            this.panel26.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(111, 70);
            this.panel26.TabIndex = 57;
            // 
            // panel25
            // 
            this.panel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel25.Location = new System.Drawing.Point(347, 282);
            this.panel25.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(111, 70);
            this.panel25.TabIndex = 56;
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.btnTSEList);
            this.panel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel20.Location = new System.Drawing.Point(117, 212);
            this.panel20.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(111, 66);
            this.panel20.TabIndex = 51;
            // 
            // btnTSEList
            // 
            this.btnTSEList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnTSEList.BackgroundImage")));
            this.btnTSEList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTSEList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTSEList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTSEList.Location = new System.Drawing.Point(0, 0);
            this.btnTSEList.Name = "btnTSEList";
            this.btnTSEList.Size = new System.Drawing.Size(111, 66);
            this.btnTSEList.TabIndex = 9;
            this.btnTSEList.Text = "TSE List";
            this.btnTSEList.UseVisualStyleBackColor = true;
            this.btnTSEList.Visible = false;
            this.btnTSEList.Click += new System.EventHandler(this.btnTSEList_Click);
            // 
            // panel18
            // 
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(577, 142);
            this.panel18.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(117, 66);
            this.panel18.TabIndex = 49;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.btnExtendedRangePrice);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(462, 142);
            this.panel17.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(111, 66);
            this.panel17.TabIndex = 48;
            // 
            // btnExtendedRangePrice
            // 
            this.btnExtendedRangePrice.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnExtendedRangePrice.BackgroundImage")));
            this.btnExtendedRangePrice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExtendedRangePrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnExtendedRangePrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExtendedRangePrice.Location = new System.Drawing.Point(0, 0);
            this.btnExtendedRangePrice.Name = "btnExtendedRangePrice";
            this.btnExtendedRangePrice.Size = new System.Drawing.Size(111, 66);
            this.btnExtendedRangePrice.TabIndex = 8;
            this.btnExtendedRangePrice.Text = "Extended Range Price File";
            this.btnExtendedRangePrice.UseVisualStyleBackColor = true;
            this.btnExtendedRangePrice.Visible = false;
            this.btnExtendedRangePrice.Click += new System.EventHandler(this.btnExtendedRangePrice_Click);
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.btnHazardousFile);
            this.panel16.Controls.Add(this.btnItemCard);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel16.Location = new System.Drawing.Point(347, 142);
            this.panel16.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(111, 66);
            this.panel16.TabIndex = 47;
            // 
            // btnHazardousFile
            // 
            this.btnHazardousFile.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnHazardousFile.BackgroundImage")));
            this.btnHazardousFile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHazardousFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHazardousFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHazardousFile.Location = new System.Drawing.Point(0, 0);
            this.btnHazardousFile.Name = "btnHazardousFile";
            this.btnHazardousFile.Size = new System.Drawing.Size(111, 66);
            this.btnHazardousFile.TabIndex = 7;
            this.btnHazardousFile.Text = "Hazardous File";
            this.btnHazardousFile.UseVisualStyleBackColor = true;
            this.btnHazardousFile.Visible = false;
            this.btnHazardousFile.Click += new System.EventHandler(this.btnHazardousFile_Click);
            // 
            // btnItemCard
            // 
            this.btnItemCard.BackgroundImage = global::LoginForm.Properties.Resources.gradient2;
            this.btnItemCard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnItemCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnItemCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnItemCard.Location = new System.Drawing.Point(0, 0);
            this.btnItemCard.Name = "btnItemCard";
            this.btnItemCard.Size = new System.Drawing.Size(111, 66);
            this.btnItemCard.TabIndex = 12;
            this.btnItemCard.Text = "Item Card";
            this.btnItemCard.UseVisualStyleBackColor = true;
            this.btnItemCard.Visible = false;
            this.btnItemCard.Click += new System.EventHandler(this.btnItemCard_Click);
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.btnDualUsedArticles);
            this.panel15.Controls.Add(this.btnWorker);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel15.Location = new System.Drawing.Point(232, 142);
            this.panel15.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(111, 66);
            this.panel15.TabIndex = 46;
            // 
            // btnDualUsedArticles
            // 
            this.btnDualUsedArticles.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDualUsedArticles.BackgroundImage")));
            this.btnDualUsedArticles.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDualUsedArticles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDualUsedArticles.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDualUsedArticles.Location = new System.Drawing.Point(0, 0);
            this.btnDualUsedArticles.Name = "btnDualUsedArticles";
            this.btnDualUsedArticles.Size = new System.Drawing.Size(111, 66);
            this.btnDualUsedArticles.TabIndex = 32;
            this.btnDualUsedArticles.Text = "Daul Used Articles (EUU-Licance)";
            this.btnDualUsedArticles.UseVisualStyleBackColor = true;
            this.btnDualUsedArticles.Visible = false;
            this.btnDualUsedArticles.Click += new System.EventHandler(this.btnDualUsedArticles_Click);
            // 
            // btnWorker
            // 
            this.btnWorker.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnWorker.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnWorker.BackgroundImage")));
            this.btnWorker.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnWorker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnWorker.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWorker.Location = new System.Drawing.Point(0, 0);
            this.btnWorker.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnWorker.Name = "btnWorker";
            this.btnWorker.Size = new System.Drawing.Size(111, 66);
            this.btnWorker.TabIndex = 31;
            this.btnWorker.Text = "Worker";
            this.btnWorker.UseVisualStyleBackColor = true;
            this.btnWorker.Visible = false;
            this.btnWorker.Click += new System.EventHandler(this.btnWorker_Click);
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.btnSuperDiskwithP);
            this.panel14.Controls.Add(this.btnLogin);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel14.Location = new System.Drawing.Point(117, 142);
            this.panel14.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(111, 66);
            this.panel14.TabIndex = 45;
            // 
            // btnSuperDiskwithP
            // 
            this.btnSuperDiskwithP.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSuperDiskwithP.BackgroundImage")));
            this.btnSuperDiskwithP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSuperDiskwithP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSuperDiskwithP.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuperDiskwithP.Location = new System.Drawing.Point(0, 0);
            this.btnSuperDiskwithP.Name = "btnSuperDiskwithP";
            this.btnSuperDiskwithP.Size = new System.Drawing.Size(111, 66);
            this.btnSuperDiskwithP.TabIndex = 29;
            this.btnSuperDiskwithP.Text = "Superdisk with P";
            this.btnSuperDiskwithP.UseVisualStyleBackColor = true;
            this.btnSuperDiskwithP.Visible = false;
            this.btnSuperDiskwithP.Click += new System.EventHandler(this.btnSuperDiskwithP_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnLogin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLogin.BackgroundImage")));
            this.btnLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogin.Location = new System.Drawing.Point(0, 0);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(111, 66);
            this.btnLogin.TabIndex = 28;
            this.btnLogin.Text = "Login Panel";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Visible = false;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // panel13
            // 
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(2, 142);
            this.panel13.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(111, 66);
            this.panel13.TabIndex = 44;
            // 
            // panel12
            // 
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(577, 72);
            this.panel12.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(117, 66);
            this.panel12.TabIndex = 43;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.btnDiscontinuedList);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(462, 72);
            this.panel11.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(111, 66);
            this.panel11.TabIndex = 42;
            // 
            // btnDiscontinuedList
            // 
            this.btnDiscontinuedList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDiscontinuedList.BackgroundImage")));
            this.btnDiscontinuedList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDiscontinuedList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDiscontinuedList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDiscontinuedList.Location = new System.Drawing.Point(0, 0);
            this.btnDiscontinuedList.Name = "btnDiscontinuedList";
            this.btnDiscontinuedList.Size = new System.Drawing.Size(111, 66);
            this.btnDiscontinuedList.TabIndex = 4;
            this.btnDiscontinuedList.Text = "Discontinued List";
            this.btnDiscontinuedList.UseVisualStyleBackColor = true;
            this.btnDiscontinuedList.Visible = false;
            this.btnDiscontinuedList.Click += new System.EventHandler(this.btnDiscontinuedList_Click);
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(347, 72);
            this.panel9.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(111, 66);
            this.panel9.TabIndex = 41;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.btnSlidingPriceList);
            this.panel10.Controls.Add(this.btnSupplier);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(111, 66);
            this.panel10.TabIndex = 33;
            // 
            // btnSlidingPriceList
            // 
            this.btnSlidingPriceList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSlidingPriceList.BackgroundImage")));
            this.btnSlidingPriceList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSlidingPriceList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSlidingPriceList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSlidingPriceList.Location = new System.Drawing.Point(0, 0);
            this.btnSlidingPriceList.Name = "btnSlidingPriceList";
            this.btnSlidingPriceList.Size = new System.Drawing.Size(111, 66);
            this.btnSlidingPriceList.TabIndex = 31;
            this.btnSlidingPriceList.Text = "Sliding Price List";
            this.btnSlidingPriceList.UseVisualStyleBackColor = true;
            this.btnSlidingPriceList.Visible = false;
            this.btnSlidingPriceList.Click += new System.EventHandler(this.btnSlidingPriceList_Click);
            // 
            // btnSupplier
            // 
            this.btnSupplier.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSupplier.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSupplier.BackgroundImage")));
            this.btnSupplier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSupplier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSupplier.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSupplier.Location = new System.Drawing.Point(0, 0);
            this.btnSupplier.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSupplier.Name = "btnSupplier";
            this.btnSupplier.Size = new System.Drawing.Size(111, 66);
            this.btnSupplier.TabIndex = 30;
            this.btnSupplier.Text = "Supplier";
            this.btnSupplier.UseVisualStyleBackColor = true;
            this.btnSupplier.Visible = false;
            this.btnSupplier.Click += new System.EventHandler(this.btnSupplier_Click);
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnSuperDisk);
            this.panel8.Controls.Add(this.btnQuotation);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(232, 72);
            this.panel8.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(111, 66);
            this.panel8.TabIndex = 40;
            // 
            // btnSuperDisk
            // 
            this.btnSuperDisk.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSuperDisk.BackgroundImage")));
            this.btnSuperDisk.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSuperDisk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSuperDisk.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSuperDisk.Location = new System.Drawing.Point(0, 0);
            this.btnSuperDisk.Name = "btnSuperDisk";
            this.btnSuperDisk.Size = new System.Drawing.Size(111, 66);
            this.btnSuperDisk.TabIndex = 30;
            this.btnSuperDisk.Text = "Super Disk";
            this.btnSuperDisk.UseVisualStyleBackColor = true;
            this.btnSuperDisk.Visible = false;
            this.btnSuperDisk.Click += new System.EventHandler(this.btnSuperDisk_Click);
            // 
            // btnQuotation
            // 
            this.btnQuotation.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnQuotation.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnQuotation.BackgroundImage")));
            this.btnQuotation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnQuotation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnQuotation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQuotation.Location = new System.Drawing.Point(0, 0);
            this.btnQuotation.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnQuotation.Name = "btnQuotation";
            this.btnQuotation.Padding = new System.Windows.Forms.Padding(12, 13, 12, 13);
            this.btnQuotation.Size = new System.Drawing.Size(111, 66);
            this.btnQuotation.TabIndex = 29;
            this.btnQuotation.Text = "Quotation";
            this.btnQuotation.UseVisualStyleBackColor = true;
            this.btnQuotation.Visible = false;
            this.btnQuotation.Click += new System.EventHandler(this.btnQuotation_Click);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.btnOnSale);
            this.panel7.Controls.Add(this.btnCustomer);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(117, 72);
            this.panel7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(111, 66);
            this.panel7.TabIndex = 39;
            // 
            // btnOnSale
            // 
            this.btnOnSale.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnOnSale.BackgroundImage")));
            this.btnOnSale.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOnSale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOnSale.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOnSale.Location = new System.Drawing.Point(0, 0);
            this.btnOnSale.Name = "btnOnSale";
            this.btnOnSale.Size = new System.Drawing.Size(111, 66);
            this.btnOnSale.TabIndex = 28;
            this.btnOnSale.Text = "OnSale File";
            this.btnOnSale.UseVisualStyleBackColor = true;
            this.btnOnSale.Visible = false;
            this.btnOnSale.Click += new System.EventHandler(this.btnOnSale_Click);
            // 

            // btnCustomer
            // 
            this.btnCustomer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnCustomer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCustomer.BackgroundImage")));
            this.btnCustomer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCustomer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustomer.Location = new System.Drawing.Point(0, 0);
            this.btnCustomer.Margin = new System.Windows.Forms.Padding(4);
            this.btnCustomer.Name = "btnCustomer";
            this.btnCustomer.Size = new System.Drawing.Size(148, 83);
            this.btnCustomer.TabIndex = 27;
            this.btnCustomer.Text = "Customer";
            this.btnCustomer.UseMnemonic = false;
            this.btnCustomer.UseVisualStyleBackColor = true;
            this.btnCustomer.Visible = false;
            this.btnCustomer.Click += new System.EventHandler(this.btnCustomerMain_Click);

            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(2, 72);
            this.panel6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(111, 66);
            this.panel6.TabIndex = 38;
            // 
            // panel5
            // 
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(577, 2);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(117, 66);
            this.panel5.TabIndex = 37;
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(462, 2);
            this.panel4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(111, 66);
            this.panel4.TabIndex = 36;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(347, 2);
            this.panel3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(111, 66);
            this.panel3.TabIndex = 35;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(232, 2);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(111, 66);
            this.panel2.TabIndex = 34;
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(117, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(111, 66);
            this.panel1.TabIndex = 33;
            // 
            // dock1
            // 
            this.dock1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dock1.Location = new System.Drawing.Point(2, 2);
            this.dock1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dock1.Name = "dock1";
            this.dock1.Size = new System.Drawing.Size(111, 66);
            this.dock1.TabIndex = 32;
            // 
            // panel19
            // 
            this.panel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel19.Location = new System.Drawing.Point(2, 212);
            this.panel19.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(111, 66);
            this.panel19.TabIndex = 50;
            // 
            // panel24
            // 
            this.panel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel24.Location = new System.Drawing.Point(577, 212);
            this.panel24.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(117, 66);
            this.panel24.TabIndex = 55;
            // 
            // panel23
            // 
            this.panel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel23.Location = new System.Drawing.Point(462, 212);
            this.panel23.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(111, 66);
            this.panel23.TabIndex = 54;
            // 
            // panel22
            // 
            this.panel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel22.Location = new System.Drawing.Point(347, 212);
            this.panel22.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(111, 66);
            this.panel22.TabIndex = 53;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.btnRSProList);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel21.Location = new System.Drawing.Point(232, 212);
            this.panel21.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(111, 66);
            this.panel21.TabIndex = 52;
            // 
            // btnRSProList
            // 
            this.btnRSProList.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRSProList.BackgroundImage")));
            this.btnRSProList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRSProList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRSProList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRSProList.Location = new System.Drawing.Point(0, 0);
            this.btnRSProList.Name = "btnRSProList";
            this.btnRSProList.Size = new System.Drawing.Size(111, 66);
            this.btnRSProList.TabIndex = 10;
            this.btnRSProList.Text = "RS Pro List";
            this.btnRSProList.UseVisualStyleBackColor = true;
            this.btnRSProList.Visible = false;
            this.btnRSProList.Click += new System.EventHandler(this.btnRSProList_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 358);
            this.Controls.Add(this.tableMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MinimumSize = new System.Drawing.Size(866, 395);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "IME General Components";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.tableLeftPanel.ResumeLayout(false);
            this.gbMainMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tableMain.ResumeLayout(false);
            this.tableRightPanel.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLeftPanel;
        private System.Windows.Forms.TableLayoutPanel tableMain;
        private System.Windows.Forms.GroupBox gbMainMenu;
        private System.Windows.Forms.Button btnDevelopment;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnLoader;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TableLayoutPanel tableRightPanel;
        private System.Windows.Forms.Button btnCustomer;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnQuotation;
        private System.Windows.Forms.Button btnSupplier;
        private System.Windows.Forms.Button btnWorker;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel dock1;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Button btnOnSale;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Button btnSuperDisk;
        private System.Windows.Forms.Button btnSlidingPriceList;
        private System.Windows.Forms.Button btnDiscontinuedList;
        private System.Windows.Forms.Button btnSuperDiskwithP;
        private System.Windows.Forms.Button btnDualUsedArticles;
        private System.Windows.Forms.Button btnHazardousFile;
        private System.Windows.Forms.Button btnExtendedRangePrice;
        private System.Windows.Forms.Button btnTSEList;
        private System.Windows.Forms.Button btnRSProList;
        private System.Windows.Forms.Button btnItemCard;
        private System.Windows.Forms.Panel panel5;
    }
}